/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee;

import java.util.Date;

/**
 *
 * @author Adeel Khilji
 */
public class Demo 
{
    public static void main(String[] args)
    {
        Date date = new Date();
        Employee employee = new Employee("1002D","Jane Doe", date);
        Tool tool = new Tool();
        String promotion = "";
        if(tool.isPromotionDueThisYear() == false)
        {
            promotion = "NO";
        }
        System.out.println("EMPLOYEE ID: " + employee.getEmployeeId() + " EMPLOYEE NAME: " + employee.getEmployeeName() + " JOIN DATE: " + employee.getEmployeeDateOfJoining());
        System.out.println("INCOME TAX: " + tool.calcIncomeTaxForCurrentYear(30000) + " PROMOTION EXPECTED: " + promotion);
        
    
    
    }
}
