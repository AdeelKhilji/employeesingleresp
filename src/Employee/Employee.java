/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee;

import java.util.Date;

/**
 *
 * @author Adeel Khilji
 */
public class Employee 
{
    private String employeeId;
    private String name;
    private Date dateOfJoining;
    
    public Employee(String employeeId, String name, Date dateOfJoining)
    {
        this.employeeId = employeeId;
        this.name = name;
        this.dateOfJoining = dateOfJoining;
    }
    
    public String getEmployeeId()
    {
        return this.employeeId;
    }
    
    public String getEmployeeName()
    {
        return this.name;
    }
    
    public Date getEmployeeDateOfJoining()
    {
        return this.dateOfJoining;
    }
}
